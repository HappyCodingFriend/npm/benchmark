# benchmark

JavaScript benchmark library

## Installation

```shell
$ npm install @happycodingfriend/benchmark
```

## Example

```js
const Benkchmark = require('@happycodingfriend/benchmark')
const benkchmark = new Benkchmark({ times: 50 })

function add(a, b) {
    return new Promise(function (resolve) {
        setTimeout(() => {
            resolve(a + b)
        }, 50)
    })
}

benkchmark.test(add, 20, 30)
    .then(console.table)
    
/*
┌─────────┬──────────┐
│ (index) │  Values  │
├─────────┼──────────┤
│  times  │    50    │
│   avg   │ '50.2ms' │
│  stdev  │ '0.40ms' │
│ median  │  '50ms'  │
│   min   │  '50ms'  │
│   max   │  '51ms'  │
└─────────┴──────────┘
*/
```
