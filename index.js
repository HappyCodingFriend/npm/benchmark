const assert = require('assert')

class Benkcmark {

    /**
     * Creat benkcmark
     * @param {number} times - Number of executions
     * @param {string} timer - timer type (hrtime or now)
     */
    constructor({ times = 5 }) {
        assert.ok(times > 0)

        this.times = times
    }

    /**
     * Benkcmark function
     * @param {Function} func - tested function
     * @param {...any} arg - tested function arg
     */
    test(func, ...arg) {
        assert.ok(func instanceof Function)

        return new Promise(async (resolve, reject) => {
            const results = []
            for (let i = 0; i < this.times; i++) {
                try {
                    const start = Date.now()
                    await func(...arg)
                    const end = Date.now()

                    results.push(end - start)
                }
                catch (err) {
                    reject(err)
                }
            }
            resolve(this.analysis(results))
        })
    }

    analysis(results) {

        results = results.sort((a, b) => a - b)

        const sum = results.reduce((a, b) => b += a)
        const avg = sum / results.length

        const stdev = Math.sqrt(results.reduce((a, b) => ((b - avg) ** 2) + a, 0) / results.length).toFixed(2)

        const lowMiddle = Math.floor((results.length - 1) / 2)
        const highMiddle = Math.ceil((results.length - 1) / 2)
        const median = (results[lowMiddle] + results[highMiddle]) / 2

        const min = results[0]
        const max = results[results.length - 1]

        return {
            times: this.times,
            avg: avg + 'ms',
            stdev: stdev + 'ms',
            median: median + 'ms',
            min: min + 'ms',
            max: max + 'ms',
        }
    }
}

module.exports = Benkcmark