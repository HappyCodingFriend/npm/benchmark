const Benkcmark = require('./index')
const benkcmark = new Benkcmark({ times: 50 })

function add(a, b) {
    return new Promise(function (resolve) {
        setTimeout(() => {
            resolve(a + b)
        }, 50)
    })
}

benkcmark.test(add, 20, 30)
    .then(console.table)